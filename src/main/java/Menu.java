public enum Menu {
    ADD(1), GET(2), REMOVE(3), SIZE(4), PRINT(5), SHOW(6), END(7);

    private int n;

    Menu(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public static Menu getElement(int n) {
        for(int i = 0; i < 7; i++) {
            if (n == Menu.values()[i].n){
                return Menu.values()[i];
            }
        }
        return null;
    }
}
