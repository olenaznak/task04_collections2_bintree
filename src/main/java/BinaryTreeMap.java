import java.util.*;

public class BinaryTreeMap<K, V> implements Map<K, V> {

    Entry<K, V> root;
    int size;

    public Entry<K, V> getRoot() {
        return root;
    }

    public void setRoot(Entry<K, V> root) {
        this.root = root;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    static final class Entry<K, V> implements Map.Entry<K, V> {
        K key;
        V value;
        Entry<K, V> left;
        Entry<K, V> right;
        Entry<K, V> parent;

        public void setKey(K key) {
            this.key = key;
        }

        Entry(K key, V value, Entry<K, V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;

            return valEquals(key, e.getKey()) && valEquals(value, e.getValue());
        }

        public int hashCode() {
            int keyHash = (key == null ? 0 : key.hashCode());
            int valueHash = (value == null ? 0 : value.hashCode());
            return keyHash ^ valueHash;
        }

        public String toString() {
            return key + " - " + value;
        }
    }

    static final boolean valEquals(Object o1, Object o2) {
        return (o1 == null ? o2 == null : o1.equals(o2));
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    public Entry<K, V> getEntry(Object key) {
        if (key == null) {
            throw new NullPointerException();
        }
        Comparable<? super K> k = (Comparable<? super K>) key;
        Entry<K, V> p = root;
        while (p != null) {
            int cmp = k.compareTo(p.key);
            if (cmp < 0)
                p = p.left;
            else if (cmp > 0)
                p = p.right;
            else
                return p;
        }
        return null;
    }

    public V remove(Object key) {
        Comparable<? super K> k = (Comparable<? super K>) key;
        Entry<K, V> current = root;
        while (current != null) {
            int cmp = k.compareTo(current.key);
            if (cmp < 0) {
                current = current.left;
            } else if (cmp > 0) {
                current = current.right;
            } else {
                break;
            }
        }
        V val = current.value;
        int type = defineSubEntries(current);
        switch (type) {
            case 0:
                if (current == root) {
                    root = null;
                } else if (current == current.parent.left) {
                    current.parent.left = null;
                } else {
                    current.parent.right = null;
                }
                break;
            case 1:
                if (current == root) {
                    root = root.right;
                } else if (current == current.parent.left) {
                    current.parent.left = current.right;
                } else {
                    current.parent.right = current.right;
                }
                break;
            case -1:
                if (current == root) {
                    root = root.left;
                } else if (current == current.parent.left) {
                    current.parent.left = current.left;
                } else {
                    current.parent.right = current.left;
                }
                break;
            case 2:
                Entry<K, V> node = current.left;
                while (node.right != null) {
                    node = node.right;
                }
                node.right = current.right;
                if (node != current.left) {
                    node.parent.right = node.left;
                    node.left = current.left;
                }
                if (current == root) {
                    root = node;
                } else if (current == current.parent.left) {
                    current.parent.left = node;
                } else {
                    current.parent.right = node;
                }
        }
        size--;
        return val;
    }

    private int defineSubEntries(Entry<K, V> el) {
        if (el.right == null && el.left == null) { //  no subtrees
            return 0;
        }
        if (el.right != null && el.left != null) { //  two subtrees
            return 2;
        }
        if (el.right != null) { // only right subtree
            return 1;
        }
        return -1; // only left subtree
    }

    public V put(K key, V value) {
        Entry<K, V> t = root;
        if (t == null) {
            root = new Entry<K, V>(key, value, null);
            size = 1;
            return value;
        }
        Comparable<? super K> k = (Comparable<? super K>) key;
        Entry<K, V> parent;
        int cmp;
        do {
            parent = t;
            cmp = k.compareTo(t.key);
            if (cmp == 0) {
                return null;
            }
            if (cmp < 0)
                t = t.left;
            else {
                t = t.right;
            }
        } while (t != null);
        Entry<K, V> e = new Entry<K, V>(key, value, parent);
        if (cmp < 0)
            parent.left = e;
        else
            parent.right = e;
        return value;
    }


    public void clear() {
        size = 0;
        root = null;
    }

    public void printTree() {
        print(root);
    }

    private void print(Entry<K, V> rt) {
        if (rt == null) {
            return;
        }
        print(rt.left);
        System.out.println(rt.key + "  " + rt.value);
        print(rt.right);
    }


    public void showTree() {
        show(root, 1);
    }

    private void show(Entry<K, V> rt, int l) {
        if (rt == null) {
            return;
        }
        show(rt.left, l + 1);
        for (int i = 0; i < l * 8; i++) {
            System.out.printf("%c", ' ');
        }
        System.out.printf("%s\n", rt.value);
        show(rt.right, l + 1);
    }

    public Set keySet() {
        return null;
    }

    public Collection values() {
        return null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return null;
    }

    public void putAll(Map m) {
    }

    public boolean containsKey(Object key) {  ///////////////
        return false;
    }

    public boolean containsValue(Object value) {  //////////////
        return false;
    }

    public V get(Object key) {
        return null;
    }
}
