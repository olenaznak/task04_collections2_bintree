import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BinaryTreeMap<Integer, String> btm = new BinaryTreeMap<Integer, String>();
        int choice;
        showMenu();
        M1:
        do {
            System.out.println("Choose option");
            choice = getInt();
            switch (Menu.getElement(choice)) {
                case ADD:
                    System.out.println("Enter key and value");
                    btm.put(getInt(), getString());
                    break;
                case GET:
                    System.out.println("Enter key");
                    System.out.println(btm.getEntry(getInt()));
                    break;
                case REMOVE:
                    System.out.println("Enter key");
                    btm.remove(getInt());
                    break;
                case SIZE:
                    System.out.println(btm.getSize());
                    break;
                case PRINT:
                    btm.printTree();
                    break;
                case SHOW:
                    btm.showTree();
                    break;
                case END:
                    break M1;

            }
        } while (true);
   /*     btm.put(3, "Node 3");
        btm.put(10, "Node 10");
        btm.put(5, "Node 5");
        btm.put(1, "Node 1");
        btm.put(6, "Node 6");
        btm.put(0, "Node 0");
        btm.put(34, "Node 34");
        btm.put(15, "Node 15");

        btm.printTree();

        btm.showTree();

        btm.remove(10);

        btm.printTree();

        btm.showTree();*/
    }

    private static void showMenu() {
        System.out.println("Input:");
        System.out.println(Menu.ADD.getN() + " - add node");
        System.out.println(Menu.GET.getN() + " - get node");
        System.out.println(Menu.REMOVE.getN() + " - remove node");
        System.out.println(Menu.SIZE.getN() + " - print tree");
        System.out.println(Menu.PRINT.getN() + " - show tree");
        System.out.println(Menu.SHOW.getN() + " - end task");
        System.out.println(Menu.END.getN() + " - end task");

    }

    public static int getInt() {
        Scanner in = new Scanner(System.in);
        while (!in.hasNextInt()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        return in.nextInt();
    }

    public static String getString() {
        String str;
        Scanner in = new Scanner(System.in);
        while (!in.hasNextLine()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        str = in.nextLine();
        return str;
    }
}
